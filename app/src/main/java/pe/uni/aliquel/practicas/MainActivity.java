package pe.uni.aliquel.practicas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
//import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    RadioButton radio1, radio2,radio3;
    Button boton_init;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radio1 = findViewById(R.id.radio_boton_1);
        radio2 = findViewById(R.id.radio_boton_2);
        radio3 = findViewById(R.id.radio_boton_3);
        boton_init = findViewById(R.id.button_start);

        boton_init.setOnClickListener(v->{
            if (!radio1.isChecked() && !radio2.isChecked() && !radio3.isChecked()){
                Snackbar.make(v, R.string.snackbar, Snackbar.LENGTH_LONG).show();
            }

            if(radio1.isChecked()){
                Intent intent = new Intent(MainActivity.this, BasicActivity.class);
                intent.putExtra("one", true);
                startActivity(intent);
            }
            if (radio2.isChecked() || radio3.isChecked()){
                //Toast.makeText(getApplicationContext(), R.string.message_toast, Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.titulo_dialog);
                builder.setCancelable(false);
                builder.setMessage(R.string.message_dialog);
                builder.setPositiveButton("SI", (dialog, which) -> {
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("No", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();

            }



        });
    }
}