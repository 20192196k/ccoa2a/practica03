package pe.uni.aliquel.practicas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")

public class SplashActivity extends AppCompatActivity {
    ImageView imageSplash;
    TextView textview;
    Animation animImage, animText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imageSplash = findViewById(R.id.logo);
        textview = findViewById(R.id.text_view_splash);
        animImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);
        animText = AnimationUtils.loadAnimation(this, R.anim.text_animation);

        imageSplash.setAnimation(animImage);
        textview.setAnimation(animText);

        new CountDownTimer(6000, 1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this,  MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }

}
